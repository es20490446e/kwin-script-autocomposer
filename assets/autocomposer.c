#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xatom.h>
#include <X11/Xlib.h>


int       main();
Window    activeWindow(Display *display, Window root);
int       checkSession();
Window    createBlocker(Display* display, int screen);
Window    createBlockerWindow(Display* display, int screen);
Display*  openDisplay();
int       setBlockerProperties(Display* display, Window blocker);
int       showBlocker(Display* display, int screen, Window blocker);
bool      windowBlocksComposition(Display* display, Window window);
bool      windowBypassesCompositor(Display* display, Window window);
bool      windowIsFullscreen(Display* display, Window window);


int main() {
	checkSession();

	Display* display = openDisplay();
	int screen = DefaultScreen(display);
	Window root = RootWindow(display, screen);
	Window blocker = createBlocker(display, screen);
	XSelectInput(display, root, PropertyChangeMask);

	XEvent event;
	Window active = None;
	Window oldActive = None;
	bool blocked = false;

	while(true) {
		XNextEvent(display, &event);

		if (event.type == PropertyNotify && event.xproperty.atom == XInternAtom(display, "_NET_ACTIVE_WINDOW", False)) {
			oldActive = active;
			active = activeWindow(display, root);

			if (active != oldActive && active != blocker && active != None) {
				if (windowBlocksComposition(display, active)) {
					showBlocker(display, screen, blocker);
					blocked = true;
				} else if (blocked) {
					XDestroyWindow(display, blocker);
					blocker = createBlocker(display, screen);
					blocked = false;
				}
			}
		}
	}
}


Window activeWindow(Display *display, Window root) {
	Window result = None;

	Atom property = XInternAtom(display, "_NET_ACTIVE_WINDOW", False);
	long offset = 0;
	long length = (~0L);
	bool deleteProperty = False;
	Atom requestedType = XA_WINDOW;
	Atom returnedType;
	int returnedFormat;
	unsigned long returnedCount;
	unsigned long nonReadBytes;
	unsigned char* returnedProperties;

	int hasProperties = XGetWindowProperty(
		display,
		root,
		property,
		offset,
		length,
		deleteProperty,
		requestedType,
		&returnedType,
		&returnedFormat,
		&returnedCount,
		&nonReadBytes,
		&returnedProperties
	);

	if (returnedCount == 1) {
		result = ((Atom *)returnedProperties)[0];
	}

	XFree(returnedProperties);
	return result;
}


int checkSession() {
	char *session = getenv("XDG_SESSION_TYPE");

	if (session == NULL || strcmp(session, "x11") != 0) {
		exit(0);
	}
}


Window createBlocker(Display* display, int screen) {
	Window blocker = createBlockerWindow(display, screen);
	setBlockerProperties(display, blocker);
	return blocker;
}


Window createBlockerWindow(Display* display, int screen) {
	Window window = XCreateSimpleWindow(
		display,
		RootWindow(display, screen),
		0, 0, 100, 100, 4,
		BlackPixel(display, screen),
		WhitePixel(display, screen)
	);

	return window;
}


Display* openDisplay() {
	Display *display = XOpenDisplay(NULL);

	if (!display) {
		fprintf(stderr, "No display\n");
		exit(EXIT_FAILURE);
	}

	return display;
}


int setBlockerProperties(Display* display, Window blocker) {
	XChangeProperty(
		display,
		blocker,
		XInternAtom(display, "_NET_WM_BYPASS_COMPOSITOR", False),
		XA_CARDINAL, 32,
		PropModeReplace,
		(unsigned char*) (long[]){1}, 1
	);

	Atom stateAbove = XInternAtom(display, "_NET_WM_STATE_SKIP_PAGER", False);
	XChangeProperty(
		display,
		blocker,
		XInternAtom(display, "_NET_WM_STATE_SKIP_PAGER", False),
		XA_ATOM, 32,
		PropModeReplace,
		(unsigned char *) &stateAbove, 1
	);

	Atom skipTaskbar = XInternAtom(display, "_NET_WM_STATE_SKIP_TASKBAR", False);
	XChangeProperty(
		display,
		blocker,
		XInternAtom(display, "_NET_WM_STATE", False),
		XA_ATOM, 32,
		PropModeReplace,
		(unsigned char *) &skipTaskbar, 1
	);
}


int showBlocker(Display* display, int screen, Window blocker) {
	XMapWindow(display, blocker);
	XIconifyWindow(display, blocker, screen);
	XFlush(display);
}


bool windowBlocksComposition(Display* display, Window window) {
	bool result = False;

	if (windowIsFullscreen(display, window) && !windowBypassesCompositor(display, window)) {
		result = True;
	}

	return result;
}


bool windowBypassesCompositor(Display* display, Window window) {
	bool result = False;

	Atom property = XInternAtom(display, "_NET_WM_BYPASS_COMPOSITOR", False);
	long offset = 0;
	long length = 1;
	bool deleteProperty = False;
	Atom requestedType = XA_CARDINAL;
	Atom returnedType;
	int returnedFormat;
	unsigned long returnedCount;
	unsigned long nonReadBytes;
	unsigned char* returnedProperties;

	int hasProperties = XGetWindowProperty(
		display,
		window,
		property,
		offset,
		length,
		deleteProperty,
		requestedType,
		&returnedType,
		&returnedFormat,
		&returnedCount,
		&nonReadBytes,
		&returnedProperties
	);

	if (returnedCount == 1 && ((long *)returnedProperties)[0] == 2) {
		result = True;
	}

	XFree(returnedProperties);
	return result;
}


bool windowIsFullscreen(Display* display, Window window) {
	bool result = False;

	Atom property = XInternAtom(display, "_NET_WM_STATE", False);
	long offset = 0;
	long length = (~0L);
	bool deleteProperty = False;
	Atom requestedType = XA_ATOM;
	Atom returnedType;
	int returnedFormat;
	unsigned long returnedCount;
	unsigned long nonReadBytes;
	unsigned char* returnedProperties;

	int hasProperties = XGetWindowProperty(
		display,
		window,
		property,
		offset,
		length,
		deleteProperty,
		requestedType,
		&returnedType,
		&returnedFormat,
		&returnedCount,
		&nonReadBytes,
		&returnedProperties
	);

	if (returnedCount != 0) {
		unsigned long index = 0;

		while (index < returnedCount && result == 0) {
			Atom returnedAtom = ((Atom *)returnedProperties)[index];
			Atom fullscreen = XInternAtom(display, "_NET_WM_STATE_FULLSCREEN", False);

			if (returnedAtom == fullscreen) {
				result = True;
			} else {
				index++;
			}
		}
	}

	XFree(returnedProperties);
	return result;
}
