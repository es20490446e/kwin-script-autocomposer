#! /bin/bash
set -e

here="$(realpath "$(dirname "${0}")")"


mainFunction () {
	rm --recursive --force "${here}/_ROOT"
	makeBin
	makeMan
	makeSystemd
}


checkDependencies () {
	local missing=()

	local dependencyLists=(
		"${here}/info/dependencies.txt"
		"${here}/info/dependencies/building.txt"
		"${here}/info/dependencies/installing.txt"
	)

	for dependencyList in "${dependencyLists[@]}"; do
		if [[ -f "${dependencyList}" ]]; then
			local lines; readarray -t lines < <(awk 'NF' "${dependencyList}")

			for line in "${lines[@]}"; do
				local name; name="$(echo "${line}" | cut --delimiter='"' --fields=2)"
				local path; path="$(echo "${line}" | cut --delimiter='"' --fields=4)"
				local web; web="$(echo "${line}" | cut --delimiter='"' --fields=6)"

				if [[ -n "${web}" ]]; then
					local web="(${web})"
				fi

				if [[ ! -f "${path}" ]]; then
					missing+=("${name}  ${web}")
				fi
			done
		fi
	done

	if [[ "${#missing[@]}" -gt 0 ]]; then
		echo "Missing required software:" >&2
		echo >&2
		printf '%s\n' "${missing[@]}" >&2
		echo >&2
		echo "Get those installed first"
		echo "and run this installer again"
		exit 1
	fi
}


makeBin () {
	mkdir --parents "${here}/_ROOT/usr/bin"
	gcc "${here}/assets/autocomposer.c" -o "${here}/_ROOT/usr/bin/autocomposer" -lX11
}


makeMan () {
	mkdir --parents "${here}/_ROOT/usr/share/man/man8"
	cp "${here}/assets/autocomposer.8" "${here}/_ROOT/usr/share/man/man8/autocomposer.8"
}


makeSystemd () {
	mkdir --parents "${here}/_ROOT/usr/lib/systemd/user"
	cp "${here}/assets/autocomposer.service" "${here}/_ROOT/usr/lib/systemd/user/autocomposer.service"
}


checkDependencies
mainFunction
